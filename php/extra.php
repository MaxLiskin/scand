<?php 

include_once 'products.php';

class Book extends Products {
    protected $extraField;
    protected function extra() {
        $this->extraField = "Weight: ".$_POST['weight']." KG";
        $this->checkPrice();
    }
    public function check() {
        if (is_numeric($_POST['weight'])) {
            $this->extra();
          } else {
            echo "Please enter valid book parametres";
            exit();
          };
    }
}
class Disc extends Products {
    protected $extraField;
    protected function extra() {
        $this->extraField = "Size: ".$_POST['size']." MB";
        $this->checkPrice();
    }
    public function check() {
        if (is_numeric($_POST['size'])) {
            $this->extra();
          } else {
            echo "Please enter valid disc parametres";
            exit();
          };
    }
}
class Furniture extends Products {
    protected $extraField;
    protected function extra() {
        $this->extraField = "Dimension: ".$_POST['height']."x".$_POST['width']."x".$_POST['length'];
        $this->checkPrice();
    }
    public function check() {
        if (is_numeric($_POST['height']) && is_numeric($_POST['width']) && is_numeric($_POST['length'])) {
            $this->extra();
          } else {
            echo "Please enter valid furniture parametres";
            exit();
          };
    }
}
function checkExtra() {
    $capitalLetter = ucfirst($_POST['choose']);
    $className = new $capitalLetter();
    $className->check();
}
checkExtra();

?>